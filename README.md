# Repo pour le projet de .net - Licence pro web

## Install
- Modifier les fichiers `App.config` pour ajouter la BDD :
```
  <connectionStrings>
    <add name="JobAdDatabase" connectionString="Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=U:\Documents\DotNet\JobAd.Brugiere.Muller\Model.JobAd\Database\JobAdDatabase.mdf;Integrated Security=True" providerName="System.Data.SqlClient" />
  </connectionStrings>
```
