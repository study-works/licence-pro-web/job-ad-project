﻿using BusinessLogicLayer.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Console.JobAd
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Manager m = Manager.Instance;
                List<Offer> offers = m.GetAllOffers().ToList();

                System.Console.WriteLine(offers.Count());

                foreach (Offer offer in offers)
                {
                    System.Console.WriteLine(offer.JobTitle);
                    System.Console.WriteLine(offer.StatusID);
                    System.Console.WriteLine(offer.Status.Wording);
                }

                System.Console.WriteLine("OK!");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
