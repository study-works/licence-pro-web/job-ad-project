﻿using AdminRH.JobAd.ViewModels.Common;
using BusinessLogicLayer.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace AdminRH.JobAd.ViewModels
{
    public class OfferDetailViewModel : BaseViewModel
    {
        #region Variables

        private Manager m;
        private int _id;
        private String _title;
        private Status _status;
        private float _salary;
        private DateTime _date;
        private String _city;
        private String _manager;
        private String _description;
        private RelayCommand _updateOffer;

        #endregion

        #region Constructeurs

        /// <summary>
        /// Constructeur par défaut
        /// <param name="p">Produit à transformer en DetailProduitViewModel</param>
        /// </summary>
        public OfferDetailViewModel(Offer o)
        {
            m = BusinessLogicLayer.JobAd.Manager.Instance;
            _id = o.ID;
            _title = o.JobTitle;
            _status = o.Status;
            _salary = o.Salary;
            _date = o.Date;
            _city = o.City;
            _manager = o.Manager;
            _description = o.Description;
        }

        #endregion

        #region Data Bindings

        public string JobTitle
        {
            get { return _title; }
            set { _title = value; }
        }

        public Status Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public float Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string Manager
        {
            get { return _manager; }
            set { _manager = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region Commandes

        /// <summary>
        /// Commande pour ouvrir la fenêtre pour ajouter une opération
        /// </summary>
        public ICommand UpdateOffer
        {
            get
            {
                if (_updateOffer == null)
                    _updateOffer = new RelayCommand(() => this.SaveOffer());
                return _updateOffer;
            }
        }

        private void SaveOffer()
        {
            try
            {
                m.UpdateOffer(new Offer { ID = _id, JobTitle = _title, City = _city, Date = _date, Description = _description, Manager = _manager, Salary = _salary, StatusID = _status.ID });
                MessageBox.Show("L'offre a bien été mise à jour.", "Mise à jour");
            } catch (Exception e)
            {
                MessageBox.Show($"Une erreur est survenur, l'offre n'a pas été mise à jour : {e.Message}", "Mise à jour");
            }
        }

        #endregion
    }
}
