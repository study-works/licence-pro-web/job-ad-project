﻿using AdminRH.JobAd.ViewModels.Common;
using BusinessLogicLayer.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminRH.JobAd.ViewModels
{
    public class OfferListViewModel : BaseViewModel
    {
        #region Variables

        private Manager m;
        private ObservableCollection<OfferDetailViewModel> _offers = null;
        private OfferDetailViewModel _selectedOffer;

        #endregion

        #region Constructeurs

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public OfferListViewModel()
        {
            m = Manager.Instance;
            m.GetAllStatus();

            _offers = new ObservableCollection<OfferDetailViewModel>();

            foreach (Offer o in m.GetAllOffersOrderByDate())
            {
                _offers.Add(new OfferDetailViewModel(o));
            }

            if (_offers != null && _offers.Count > 0)
                _selectedOffer = _offers.ElementAt(0);
        }

        #endregion

        #region Data Bindings

        /// <summary>
        /// Obtient ou définit une collection de OfferDetailViewModel (Observable)
        /// </summary>
        public ObservableCollection<OfferDetailViewModel> Offers
        {
            get { return _offers; }
            set
            {
                _offers = value;
                OnPropertyChanged("Offers");
            }
        }

        /// <summary>
        /// Obtient ou défini le produit en cours de sélection dans la liste de OfferDetailViewModel
        /// </summary>
        public OfferDetailViewModel SelectedOffer
        {
            get { return _selectedOffer; }
            set
            {
                _selectedOffer = value;
                OnPropertyChanged("SelectedOffer");
            }
        }

        #endregion
    }
}
