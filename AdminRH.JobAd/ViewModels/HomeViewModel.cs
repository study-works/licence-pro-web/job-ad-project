﻿using AdminRH.JobAd.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminRH.JobAd.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        #region Variables

        private OfferListViewModel _offerListViewModel = null;

        #endregion

        #region Constructeurs

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public HomeViewModel()
        {
            _offerListViewModel = new OfferListViewModel();
        }

        #endregion

        #region Data Bindings

        /// <summary>
        /// Obtient ou définit le ListeProduitViewModel
        /// </summary>
        public OfferListViewModel OfferListViewModel
        {
            get { return _offerListViewModel; }
            set { _offerListViewModel = value; }
        }

        #endregion
    }
}
