﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientRH.Models;
using BusinessLogicLayer.JobAd;
using System.Threading.Tasks;
using Model.JobAd;
using System.Data.Entity;
using Model.JobAd.Entity;

namespace ClientRH.Controllers
{
    public class OfferController : Controller
    {
        Manager m = Manager.Instance;
        private readonly Context _context;
        // GET: Offer
        [ActionName("ListOffers")]
        public async Task<ActionResult> Index(string searchString)
        {
            List<OfferViewModel> offers = new List<OfferViewModel>();
            List<Offer> l = m.GetAllOffers().ToList();
            if (!String.IsNullOrEmpty(searchString))
            {
                l = l.ToList().Where(o => o.JobTitle.Contains(searchString)).ToList();
            }
            foreach (Offer o in l)
            {
                offers.Add(new OfferViewModel { City = o.City, Date = o.Date, Description = o.Description, ID = o.ID, JobTitle = o.JobTitle, Manager = o.Manager, Salary = o.Salary, Status = new StatusViewModel { ID = o.Status.ID, Wording = o.Status.Wording } });
            }
            return View(offers);
        }

        // GET: Offer/Details/_id
        [ActionName("OfferDetails")]
        public ActionResult Details(int id)
        {
            Offer o = m.GetOfferByID(id);
            OfferViewModel offer = new OfferViewModel { City = o.City, Date = o.Date, Description = o.Description, ID = o.ID, JobTitle = o.JobTitle, Manager = o.Manager, Salary = o.Salary, Status = new StatusViewModel { ID = o.Status.ID, Wording = o.Status.Wording } };
            return View(offer);
        }
    }
}
