﻿using System.Web.Mvc;

namespace ClientRH.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }
    }
}