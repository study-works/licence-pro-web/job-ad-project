﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientRH.Models
{
    public class StatusViewModel
    {
        public int ID { get; set; }

        public String Wording { get; set; }

        public ICollection<OfferViewModel> Offers { get; set; }
    }
}