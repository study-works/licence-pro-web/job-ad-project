﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientRH.Models
{
    public class EmployeeViewModel
    {
        public int ID { get; set; }

        public String LastName { get; set; }

        public String FisrtName { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime EmploymentDate { get; set; }

        public float Salary { get; set; }

        public String Biography { get; set; }

        public ICollection<PostulationViewModel> Postulations { get; set; }

        public ICollection<ExperienceViewModel> Experiences { get; set; }

        public ICollection<TrainingViewModel> Trainings { get; set; }
    }
}