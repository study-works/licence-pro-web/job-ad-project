﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientRH.Models
{
    public class OfferViewModel
    {
        public int ID { get; set; }

        public String JobTitle { get; set; }

        public DateTime Date { get; set; }

        public float Salary { get; set; }

        public String City { get; set; }

        public String Description { get; set; }

        public String Manager { get; set; }

        public StatusViewModel Status { get; set; }

    }
}