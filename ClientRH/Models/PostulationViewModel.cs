﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientRH.Models
{
    public class PostulationViewModel
    {
        public int OfferID { get; set; }

        public int EmployeeID { get; set; }

        public DateTime Date { get; set; }

        public String Status { get; set; }

        public OfferViewModel Offer { get; set; }

        public EmployeeViewModel Employee { get; set; }
    }
}