﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientRH.Models
{
    public class TrainingViewModel
    {
        public int ID { get; set; }

        public String TrainingTitle { get; set; }

        public String Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int EmployeeID { get; set; }

        public EmployeeViewModel Employee { get; set; }
    }
}