﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ClientRH
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Offers",
                url: "{controller}/{action}/",
                defaults: new { controller = "Offer", action = "ListOffers" }
            );
            routes.MapRoute(
                name: "OfferDetails",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Offer", action = "OfferDetails", id = UrlParameter.Optional }
            );
        }
    }
}
