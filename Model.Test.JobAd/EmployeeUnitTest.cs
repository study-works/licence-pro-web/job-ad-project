﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Model.JobAd;
using Model.JobAd.Entity;
using BusinessLogicLayer.JobAd;

namespace Model.Test.JobAd
{
    [TestClass]
    public class EmployeeUnitTest
    {

        Manager m = Manager.Instance;
        Employee e = new Employee { FisrtName = "Test", LastName = "Test", Biography = "Test biography", BirthDate = new DateTime(2020, 01, 01), EmploymentDate = new DateTime(2020, 01, 01), Salary = 1300};

        [TestInitialize]
        public void initialize()
        {
            m.InsertEmployee(e);
        }

        [TestMethod]
        public void TestInsert()
        {
            Employee insertEmployee = new Employee { FisrtName = "Test insertion", LastName = "Test", Biography = "Test biography", BirthDate = new DateTime(2020, 01, 01), EmploymentDate = new DateTime(2020, 01, 01), Salary = 1300 };
            Assert.IsNotNull(m.InsertEmployee(insertEmployee));
        }

        [TestMethod]
        public void TestListAll()
        {
            Assert.IsNotNull(m.GetAllEmployees());
        }

        [TestMethod]
        public void TestUpdate()
        {
            e.FisrtName = "Test Modifié";
            if (m.GetEmployeeByID(e.ID) == null)
            {
                Assert.Inconclusive("Nothing to update");
            }
            else
            {
                m.UpdateEmployee(e);
                Assert.AreEqual(m.GetEmployeeByID(e.ID).FisrtName, e.FisrtName);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            if (m.GetEmployeeByID(e.ID) == null)
            {
                Assert.Inconclusive("Nothing to delete");
            }
            else
            {
                m.DeleteEmployee(e);
                Assert.IsNull(m.GetEmployeeByID(e.ID));
            }
        }

    }
}
