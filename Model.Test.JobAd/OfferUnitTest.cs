﻿using BusinessLogicLayer.JobAd;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.JobAd.Entity;
using System;

namespace Model.Test.JobAd
{
    [TestClass]
    public class OfferUnitTest
    {
        Manager m = Manager.Instance;
        Offer o = new Offer{ JobTitle = "Test", City = "Clermont-Ferrand", Date = new DateTime(2020, 12, 26), Description = "description test", Manager = "Bernard", Salary = 1500, StatusID= 2 };

        [TestInitialize]
        public void initialize()
        {
            m.InsertOffer(o);
        }

        [TestMethod]
        public void TestInsert()
        {
            Offer insertOffer = new Offer { JobTitle = "Test insertion", City = "Clermont-Ferrand", Date = new DateTime(2020, 12, 26), Description = "description test", Manager = "Bernard", Salary = 1500, StatusID = 2 };
            Assert.IsNotNull(m.InsertOffer(insertOffer));
        }

        [TestMethod]
        public void TestListAll()
        {
            Assert.IsNotNull(m.GetAllOffers());
        }

        [TestMethod]
        public void TestUpdate()
        {
            o.JobTitle = "Test Modifié";
            o.City = "Lyon";
            o.Salary = (float)2.56;
            if (m.GetOfferByID(o.ID) == null)
            {
                Assert.Inconclusive("Nothing to update");
            }
            else
            {
                m.UpdateOffer(o);
                Assert.AreEqual(m.GetOfferByID(o.ID), o);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            if (m.GetOfferByID(o.ID) == null)
            {
                Assert.Inconclusive("Nothing to delete");
            }
            else
            {
                m.DeleteOffer(o);
                Assert.IsNull(m.GetOfferByID(o.ID));
            }
        }
    }
}
