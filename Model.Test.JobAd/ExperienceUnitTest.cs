﻿using BusinessLogicLayer.JobAd;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.JobAd.Entity;
using System;

namespace Model.Test.JobAd
{
    [TestClass]
    public class ExperienceUnitTest
    {
        Manager m = Manager.Instance;
        Employee employee = new Employee { FisrtName = "Robert", LastName = "Bernard", Biography = "bio", BirthDate = new DateTime(1985, 04, 13), EmploymentDate = new DateTime(2006, 07, 21), Salary = (float)2476.85 };
        Experience e = new Experience { JobTitle = "Test", Description = "Description test", EmployeeID = 1, StartDate = new DateTime(2012, 01, 01), EndDate = new DateTime(2018, 12, 31) };

        [TestInitialize]
        public void initialize()
        {
            m.InsertEmployee(employee);
            m.InsertExperience(e);
        }

        [TestMethod]
        public void TestInsert()
        {
            Experience insertExp = new Experience { JobTitle = "Test insertion", Description = "Description test", EmployeeID = 1, StartDate = new DateTime(2012, 01, 01), EndDate = new DateTime(2018, 12, 31) };
            Assert.IsNotNull(m.InsertExperience(insertExp));
        }

        [TestMethod]
        public void TestListAll()
        {
            Assert.IsNotNull(m.GetAllExperiences());
        }

        [TestMethod]
        public void TestUpdate()
        {
            e.JobTitle = "Test Modifié";
            e.Description = "description modifiée";
            if (m.GetExperienceByID(e.ID) == null)
            {
                Assert.Inconclusive("Nothing to update");
            }
            else
            {
                m.UpdateExperience(e);
                Assert.AreEqual(m.GetExperienceByID(e.ID), e);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            if (m.GetExperienceByID(e.ID) == null)
            {
                Assert.Inconclusive("Nothing to delete");
            }
            else
            {
                m.DeleteExperience(e);
                Assert.IsNull(m.GetExperienceByID(e.ID));
            }
        }
    }
}
