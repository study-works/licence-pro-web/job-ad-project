﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Model.JobAd;
using Model.JobAd.Entity;
using BusinessLogicLayer.JobAd;

namespace Model.Test.JobAd
{
    [TestClass]
    public class PostulationUnitTest
    {
        Manager m = Manager.Instance;
        Postulation p = new Postulation { EmployeeID = 1, OfferID = 9, Status = "Test", Date = new DateTime(2012, 01, 01) };

        [TestInitialize]
        public void initialize()
        {
            m.DeletePostulation(p);
            m.InsertPostulation(p);
        }

        [TestMethod]
        public void TestInsert()
        {
            Postulation insertPostulation = new Postulation { Status = "Test insertion", Date = new DateTime(2021, 01, 01), EmployeeID = 1, OfferID = 9 };
            Assert.IsNotNull(m.InsertPostulation(insertPostulation));
        }

        [TestMethod]
        public void TestListAll()
        {
            Assert.IsNotNull(m.GetAllPostulations());
        }

        [TestMethod]
        public void TestUpdate()
        {
            p.Status = "Test Modifié";
            if (m.GetPostulationByID(p.EmployeeID, p.OfferID) == null)
            {
                Assert.Inconclusive("Nothing to update");
            }
            else
            {
                m.UpdatePostulation(p);
                Assert.AreEqual(m.GetPostulationByID(p.EmployeeID, p.OfferID).Status, p.Status);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            if (m.GetPostulationByID(p.EmployeeID, p.OfferID) == null)
            {
                Assert.Inconclusive("Nothing to delete");
            }
            else
            {
                m.DeletePostulation(p);
                Assert.IsNull(m.GetPostulationByID(p.EmployeeID, p.OfferID));
            }
        }

    }
}
