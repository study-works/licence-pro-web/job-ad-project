﻿using BusinessLogicLayer.JobAd;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model.JobAd.Entity;
using System;
using Model.JobAd;

namespace Model.Test.JobAd
{
    [TestClass]
    public class TrainingUnitTest
    {

        Manager m = Manager.Instance;
        Training t = new Training{ TrainingTitle = "Test", Description = "description test", EmployeeID = 1, EndDate = new DateTime(2018, 05, 19), StartDate = new DateTime(2015, 08, 23) };

        [TestInitialize]
        public void initialize()
        {
            m.InsertTraining(t);
        }

        [TestMethod]
        public void TestInsert()
        {
            Training insertTraining = new Training { TrainingTitle = "Test insertion", Description = "description test", EmployeeID = 1, EndDate = new DateTime(2018, 05, 19), StartDate = new DateTime(2015, 08, 23) };
            Assert.IsNotNull(m.InsertTraining(insertTraining));
        }

        [TestMethod]
        public void TestListAll()
        {
            Assert.IsNotNull(m.GetAllTrainings());
        }

        [TestMethod]
        public void TestUpdate()
        {
            t.TrainingTitle = "Test Modifié";
            if (m.GetTrainingByID(t.ID) == null)
            {
                Assert.Inconclusive("Nothing to update");
            }
            else
            {
                m.UpdateTraining(t);
                Assert.AreEqual(m.GetTrainingByID(t.ID).TrainingTitle, t.TrainingTitle);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            if (m.GetTrainingByID(t.ID) == null)
            {
                Assert.Inconclusive("Nothing to delete");
            }
            else
            {
                m.DeleteTraining(t);
                Assert.IsNull(m.GetTrainingByID(t.ID));
            }
        }
    }
}
