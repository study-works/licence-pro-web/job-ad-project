﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Model.JobAd;
using Model.JobAd.Entity;
using BusinessLogicLayer.JobAd;

namespace Model.Test.JobAd
{
    [TestClass]
    public class StatusUnitTest
    {
        Manager m = Manager.Instance;
        Status s = new Status { Wording = "Test" };

        [TestInitialize]
        public void initialize()
        {
            m.InsertStatus(s);
        }

        [TestMethod]
        public void TestInsert()
        {
            Status insertStatus = new Status { Wording = "Test insertion" };
            Assert.IsNotNull(m.InsertStatus(insertStatus));
        }

        [TestMethod]
        public void TestListAll()
        {
            Assert.IsNotNull(m.GetAllStatus());
        }

        [TestMethod]
        public void TestUpdate()
        {
            s.Wording = "Test Modifié";
            if (m.GetStatusByID(s.ID) == null)
            {
                Assert.Inconclusive("Nothing to update");
            }
            else
            {
                m.UpdateStatus(s);
                Assert.AreEqual(m.GetStatusByID(s.ID).Wording, s.Wording);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            if (m.GetStatusByID(s.ID) == null)
            {
                Assert.Inconclusive("Nothing to delete");
            }
            else
            {
                m.DeleteStatus(s);
                Assert.IsNull(m.GetStatusByID(s.ID));
            }
        }
    }
}
