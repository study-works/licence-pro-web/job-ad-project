﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;
using System.Data.Entity;

namespace BusinessLogicLayer.JobAd.Queries
{
    public class TrainingQuery
    {
        private readonly Context _context;

        public TrainingQuery(Context context)
        {
            _context = context;
        }

        public IQueryable<Training> getAll()
        {
            return _context.Trainings.Include("Employee");
        }

        public IQueryable<Training> getByID(int _id)
        {
            return _context.Trainings.Where(t => t.ID == _id).Include("Employee");
        }
    }
}
