﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;

namespace BusinessLogicLayer.JobAd.Queries
{
    public class EmployeeQuery
    {
        private readonly Context _context;

        public EmployeeQuery(Context context)
        {
            _context = context;
        }

        public IQueryable<Employee> getAll()
        {
            return _context.Employees;
        }

        public IQueryable<Employee> getByID(int _id)
        {
            return _context.Employees.Where(e => e.ID == _id);
        }
    }
}
