﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;
using System.Data.Entity;

namespace BusinessLogicLayer.JobAd.Queries
{
    public class PostulationQuery
    {
        private readonly Context _context;

        public PostulationQuery(Context context)
        {
            _context = context;
        }

        public IQueryable<Postulation> getAll()
        {
            return _context.Postulations.Include("Offer").Include("Employee");
        }

        public IQueryable<Postulation> getByID(int _employeeId, int _offerId)
        {
            return _context.Postulations.Where(p => p.EmployeeID == _employeeId && p.OfferID == _offerId).Include("Offer").Include("Employee");
        }
    }
}
