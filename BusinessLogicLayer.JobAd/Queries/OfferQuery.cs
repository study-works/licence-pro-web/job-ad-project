﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;
using System.Data.Entity;

namespace BusinessLogicLayer.JobAd.Queries
{
    public class OfferQuery
    {
        private readonly Context _context;

        public OfferQuery(Context context)
        {
            _context = context;
        }
    
        public IQueryable<Offer> getAll()
        {
            return _context.Offers.Include("Status");
        }

        public IQueryable<Offer> getAllOrderByDate()
        {
            return _context.Offers.OrderByDescending(o => o.Date);
        }

        public IQueryable<Offer> getByID(int _id)
        {
            return _context.Offers.Where(o => o.ID == _id).Include("Status");
        }
    }
}
