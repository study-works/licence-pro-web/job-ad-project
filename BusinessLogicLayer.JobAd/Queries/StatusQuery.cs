﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;

namespace BusinessLogicLayer.JobAd.Queries
{
    public class StatusQuery
    {
        private readonly Context _context;

        public StatusQuery(Context context)
        {
            _context = context;
        }

        public IQueryable<Status> getAll()
        {
            return _context.Status;
        }

        public IQueryable<Status> getByID(int _id)
        {
            return _context.Status.Where(s => s.ID == _id);
        }
    }
}
