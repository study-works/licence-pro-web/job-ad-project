﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;
using System.Data.Entity;

namespace BusinessLogicLayer.JobAd.Queries
{
    public class ExperienceQuery
    {
        private readonly Context _context;

        public ExperienceQuery(Context context)
        {
            _context = context;
        }

        public IQueryable<Experience> getAll()
        {
            return _context.Experiences.Include("Employee");
        }

        public IQueryable<Experience> getByID(int _id)
        {
            return _context.Experiences.Where(e => e.ID == _id).Include("Employee");
        }
    }
}
