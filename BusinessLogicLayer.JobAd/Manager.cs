﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogicLayer.JobAd.Queries;
using BusinessLogicLayer.JobAd.Commands;
using Model.JobAd;
using Model.JobAd.Entity;

namespace BusinessLogicLayer.JobAd
{
    public class Manager
    {
        private static Manager _instance;
        private readonly Context _context;

         private Manager()
        {
            _context = new Context();
        }

        public static Manager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Manager();
                }
                return _instance;
            }
        }

        #region Offer
        public IList<Offer> GetAllOffers()
        {
            OfferQuery oq = new OfferQuery(_context);
            return oq.getAll().ToList();
        }

        public IList<Offer> GetAllOffersOrderByDate()
        {
            OfferQuery oq = new OfferQuery(_context);
            return oq.getAllOrderByDate().ToList();
        }

        public Offer GetOfferByID(int id)
        {
            OfferQuery oq = new OfferQuery(_context);
            return oq.getByID(id).FirstOrDefault();
        }

        public int InsertOffer(Offer o)
        {
            OfferCommand oc = new OfferCommand(_context);
            return oc.Insert(o);
        }

        public void UpdateOffer(Offer o)
        {
            OfferCommand oc = new OfferCommand(_context);
            oc.Update(o);
        }

        public void DeleteOffer(Offer o)
        {
            OfferCommand oc = new OfferCommand(_context);
            oc.Delete(o);
        }
        #endregion

        #region Status
        public IList<Status> GetAllStatus()
        {
            StatusQuery os = new StatusQuery(_context);
            return os.getAll().ToList();
        }

        public Status GetStatusByID(int id)
        {
            StatusQuery os = new StatusQuery(_context);
            return os.getByID(id).FirstOrDefault();
        }

        public int InsertStatus(Status s)
        {
            StatusCommand sc = new StatusCommand(_context);
            return sc.Insert(s);
        }

        public void UpdateStatus(Status s)
        {
            StatusCommand sc = new StatusCommand(_context);
            sc.Update(s);
        }

        public void DeleteStatus(Status s)
        {
            StatusCommand sc = new StatusCommand(_context);
            sc.Delete(s);
        }
        #endregion

        #region Experience
        public IList<Experience> GetAllExperiences()
        {
            ExperienceQuery oe = new ExperienceQuery(_context);
            return oe.getAll().ToList();
        }

        public Experience GetExperienceByID(int id)
        {
            ExperienceQuery oe = new ExperienceQuery(_context);
            return oe.getByID(id).FirstOrDefault();
        }

        public int InsertExperience(Experience e)
        {
            ExperienceCommand ec = new ExperienceCommand(_context);
            return ec.Insert(e);
        }

        public void UpdateExperience(Experience e)
        {
            ExperienceCommand ec = new ExperienceCommand(_context);
            ec.Update(e);
        }

        public void DeleteExperience(Experience e)
        {
            ExperienceCommand ec = new ExperienceCommand(_context);
            ec.Delete(e);
        }
        #endregion

        #region Training
        public IList<Training> GetAllTrainings()
        {
            TrainingQuery ot = new TrainingQuery(_context);
            return ot.getAll().ToList();
        }

        public Training GetTrainingByID(int id)
        {
            TrainingQuery ot = new TrainingQuery(_context);
            return ot.getByID(id).FirstOrDefault();
        }

        public int InsertTraining(Training t)
        {
            TrainingCommand tc = new TrainingCommand(_context);
            return tc.Insert(t);
        }

        public void UpdateTraining(Training t)
        {
            TrainingCommand tc = new TrainingCommand(_context);
            tc.Update(t);
        }

        public void DeleteTraining(Training t)
        {
            TrainingCommand tc = new TrainingCommand(_context);
            tc.Delete(t);
        }
        #endregion

        #region Employee
        public IList<Employee> GetAllEmployees()
        {
            EmployeeQuery oe = new EmployeeQuery(_context);
            return oe.getAll().ToList();
        }

        public Employee GetEmployeeByID(int id)
        {
            EmployeeQuery oe = new EmployeeQuery(_context);
            return oe.getByID(id).FirstOrDefault();
        }

        public int InsertEmployee(Employee e)
        {
            EmployeeCommand ec = new EmployeeCommand(_context);
            return ec.Insert(e);
        }

        public void UpdateEmployee(Employee e)
        {
            EmployeeCommand ec = new EmployeeCommand(_context);
            ec.Update(e);
        }

        public void DeleteEmployee(Employee e)
        {
            EmployeeCommand ec = new EmployeeCommand(_context);
            ec.Delete(e);
        }
        #endregion

        #region Postulation
        public IList<Postulation> GetAllPostulations()
        {
            PostulationQuery op = new PostulationQuery(_context);
            return op.getAll().ToList();
        }

        public Postulation GetPostulationByID(int employeeId, int offerId)
        {
            PostulationQuery op = new PostulationQuery(_context);
            return op.getByID(employeeId, offerId).FirstOrDefault();
        }

        public int InsertPostulation(Postulation p)
        {
            PostulationCommand pc = new PostulationCommand(_context);
            return pc.Insert(p);
        }

        public void UpdatePostulation(Postulation p)
        {
            PostulationCommand pc = new PostulationCommand(_context);
            pc.Update(p);
        }

        public void DeletePostulation(Postulation p)
        {
            PostulationCommand pc = new PostulationCommand(_context);
            pc.Delete(p);
        }
        #endregion
    }
}
