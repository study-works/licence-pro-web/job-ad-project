﻿using Model.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.JobAd.Commands
{
    class ExperienceCommand
    {

        private readonly Context _context;

        public ExperienceCommand(Context context)
        {
            _context = context;
        }

        public int Insert(Experience e)
        {
            _context.Experiences.Add(e);
            return _context.SaveChanges();
        }

        public void Update(Experience exp)
        {
            Experience upExp = _context.Experiences.Where(e => e.ID == exp.ID).FirstOrDefault();
            if (upExp != null)
            {
                upExp.JobTitle = exp.JobTitle;
                upExp.StartDate = exp.StartDate;
                upExp.Description = exp.Description;
                upExp.EmployeeID = exp.EmployeeID;
                upExp.EndDate = exp.EndDate;
            }
            _context.SaveChanges();
        }

        public void Delete(Experience exp)
        {
            Experience delExp = _context.Experiences.Where(e => e.ID == exp.ID).FirstOrDefault();
            if (delExp != null)
            {
                _context.Experiences.Remove(exp);
            }
            _context.SaveChanges();
        }
    }
}
