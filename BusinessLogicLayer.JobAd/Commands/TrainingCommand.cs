﻿using Model.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.JobAd.Commands
{
    class TrainingCommand
    {
        private readonly Context _context;

        public TrainingCommand(Context context)
        {
            _context = context;
        }

        public int Insert(Training t)
        {
            _context.Trainings.Add(t);
            return _context.SaveChanges();
        }

        public void Update(Training training)
        {
            Training upTra = _context.Trainings.Where(t => t.ID == training.ID).FirstOrDefault();
            if (upTra != null)
            {
                upTra.TrainingTitle = training.TrainingTitle;
                upTra.StartDate = training.StartDate;
                upTra.Description = training.Description;
                upTra.EmployeeID = training.EmployeeID;
                upTra.EndDate = training.EndDate;
            }
            _context.SaveChanges();
        }

        public void Delete(Training training)
        {
            Training delTra = _context.Trainings.Where(t => t.ID == training.ID).FirstOrDefault();
            if (delTra != null)
            {
                _context.Trainings.Remove(training);
            }
            _context.SaveChanges();
        }
    }
}
