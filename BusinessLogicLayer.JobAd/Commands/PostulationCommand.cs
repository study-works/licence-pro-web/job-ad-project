﻿using Model.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.JobAd.Commands
{
    class PostulationCommand
    {
        private readonly Context _context;

        public PostulationCommand(Context context)
        {
            _context = context;
        }

        public int Insert(Postulation p)
        {
            _context.Postulations.Add(p);
            return _context.SaveChanges();
        }

        public void Update(Postulation post)
        {
            Postulation upPos = _context.Postulations.Where(p => p.EmployeeID == post.EmployeeID && p.OfferID == post.OfferID).FirstOrDefault();
            if (upPos != null)
            {
                upPos.Date = post.Date;
                upPos.EmployeeID = post.EmployeeID;
                upPos.OfferID = post.OfferID;
                upPos.Status = post.Status;
            }
            _context.SaveChanges();
        }

        public void Delete(Postulation post)
        {
            Postulation delPos = _context.Postulations.Where(p => p.EmployeeID == post.EmployeeID && p.OfferID == post.OfferID).FirstOrDefault();
            if (delPos != null)
            {
                _context.Postulations.Remove(post);
            }
            _context.SaveChanges();
        }
    }
}
