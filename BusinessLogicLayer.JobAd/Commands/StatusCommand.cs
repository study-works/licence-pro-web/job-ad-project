﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;

namespace BusinessLogicLayer.JobAd.Commands
{
    public class StatusCommand
    {
        private readonly Context _context;

        public StatusCommand(Context context)
        {
            _context = context;
        }

        public int Insert(Status s)
        {
            _context.Status.Add(s);
            return _context.SaveChanges();
        }

        public void Update(Status status)
        {
            Status upStat = _context.Status.Where(s => s.ID == status.ID).FirstOrDefault();
            if (upStat != null)
            {
                upStat.Wording = status.Wording;
            }
            _context.SaveChanges();
        }

        public void Delete(Status status)
        {
            Status upStat = _context.Status.Where(s => s.ID == status.ID).FirstOrDefault();
            if (upStat != null)
            {
                _context.Status.Remove(status);
            }
            _context.SaveChanges();
        }
    }
}
