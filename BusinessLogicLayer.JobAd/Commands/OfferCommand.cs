﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.JobAd.Entity;
using Model.JobAd;

namespace BusinessLogicLayer.JobAd.Commands
{
    public class OfferCommand
    {
        private readonly Context _context;

        public OfferCommand(Context context)
        {
            _context = context;
        }

        public int Insert(Offer o)
        {
            _context.Offers.Add(o);
            return _context.SaveChanges();
        }

        public void Update(Offer offer)
        {
            Offer upOff = _context.Offers.Where(o => o.ID == offer.ID).FirstOrDefault();
            if(upOff != null)
            {
                upOff.City = offer.City;
                upOff.Date = offer.Date;
                upOff.Description = offer.Description;
                upOff.JobTitle = offer.JobTitle;
                upOff.Manager = offer.Manager;
                upOff.Salary = offer.Salary;
                upOff.StatusID = offer.StatusID;
            }
            _context.SaveChanges();
        }

        public void Delete(Offer offer)
        {
            Offer delOff = _context.Offers.Where(o => o.ID == offer.ID).FirstOrDefault();
            if(delOff != null)
            {
                _context.Offers.Remove(offer);
            }
            _context.SaveChanges();
        }
    }
}
