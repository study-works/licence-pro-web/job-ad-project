﻿using Model.JobAd;
using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.JobAd.Commands
{
    class EmployeeCommand
    {
        private readonly Context _context;

        public EmployeeCommand(Context context)
        {
            _context = context;
        }

        public int Insert(Employee e)
        {
            _context.Employees.Add(e);
            return _context.SaveChanges();
        }

        public void Update(Employee emp)
        {
            Employee upEmp = _context.Employees.Where(e => e.ID == emp.ID).FirstOrDefault();
            if (upEmp != null)
            {
                upEmp.Biography = emp.Biography;
                upEmp.BirthDate = emp.BirthDate;
                upEmp.EmploymentDate = emp.EmploymentDate;
                upEmp.FisrtName = emp.FisrtName;
                upEmp.LastName = emp.LastName;
                upEmp.Salary = emp.Salary;
            }
            _context.SaveChanges();
        }

        public void Delete(Employee emp)
        {
            Employee delEmp = _context.Employees.Where(e => e.ID == emp.ID).FirstOrDefault();
            if (delEmp != null)
            {
                _context.Employees.Remove(emp);
            }
            _context.SaveChanges();
        }
    }
}
