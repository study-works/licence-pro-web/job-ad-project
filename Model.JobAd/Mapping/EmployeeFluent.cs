﻿using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Mapping
{
    class EmployeeFluent : EntityTypeConfiguration<Employee>
    {
        public EmployeeFluent()
        {
            ToTable("APP_EMPLOYEE");
            HasKey(e => e.ID);

            Property(e => e.ID)
                .HasColumnName("EMP_ID")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.LastName)
                .HasColumnName("EMP_LASTNAME")
                .IsRequired();

            Property(e => e.FisrtName)
                .HasColumnName("EMP_FIRSTNAME")
                .IsRequired();

            Property(e => e.BirthDate)
                .HasColumnName("EMP_BIRTHDATE")
                .IsRequired();

            Property(e => e.EmploymentDate)
                .HasColumnName("EMP_EMPLOYMENTDATE")
                .IsRequired();

            Property(e => e.Salary)
                .HasColumnName("EMP_SALARY")
                .IsRequired();

            Property(e => e.Biography)
                .HasColumnName("EMP_BIOGRAPHY");
        }
    }
}
