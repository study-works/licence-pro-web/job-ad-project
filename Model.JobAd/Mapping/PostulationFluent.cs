﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Model.JobAd.Entity;

namespace Model.JobAd.Mapping
{
    public class PostulationFluent : EntityTypeConfiguration<Postulation>
    {

        public PostulationFluent()
        {
            ToTable("APP_POSTULATION");
            HasKey(p => new { p.EmployeeID, p.OfferID });

            Property(p => p.Date)
                .HasColumnName("POS_DATE")
                .IsRequired();
            
            Property(p => p.Status)
                .HasColumnName("POS_STATUS")
                .IsRequired();

            HasRequired(p => p.Employee)
                .WithMany()
                .HasForeignKey(p => p.EmployeeID);
            
            HasRequired(p => p.Offer)
                .WithMany()
                .HasForeignKey(p => p.OfferID);
        }
    }
}
