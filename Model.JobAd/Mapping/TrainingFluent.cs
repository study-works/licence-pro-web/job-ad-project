﻿using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Mapping
{
    public class TrainingFluent : EntityTypeConfiguration<Training>
    {
        public TrainingFluent()
        {
            ToTable("APP_TRAINING");
            HasKey(t => t.ID);

            Property(t => t.ID)
                .HasColumnName("TRA_ID")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.TrainingTitle)
                .HasColumnName("TRA_TRAININGTITLE")
                .IsRequired();

            Property(t => t.Description)
                .HasColumnName("TRA_DESCRIPTION")
                .IsRequired();

            Property(t => t.StartDate)
                .HasColumnName("TRA_STARTDATE")
                .IsRequired();

            Property(t => t.EndDate)
                .HasColumnName("TRA_ENDDATE")
                .IsRequired();

            HasRequired(t => t.Employee)
                .WithMany()
                .HasForeignKey(t => t.EmployeeID);
        }
    }
}
