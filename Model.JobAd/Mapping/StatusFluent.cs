﻿using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Mapping
{
    class StatusFluent : EntityTypeConfiguration<Status>
    {
        public StatusFluent()
        {
            ToTable("APP_STATUS");
            HasKey(s => s.ID);

            Property(s => s.ID)
                .HasColumnName("STA_ID")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(s => s.Wording)
                .IsRequired()
                .HasColumnName("STA_WORDING");
        }
    }
}
