﻿using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Mapping
{
    public class ExperienceFluent : EntityTypeConfiguration<Experience>
    {
        public ExperienceFluent()
        {
            ToTable("APP_EXPERIENCE");
            HasKey(e => e.ID);

            Property(e => e.ID)
                .HasColumnName("EXP_ID")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
            Property(e => e.JobTitle)
                .HasColumnName("EXP_JOBTITLE")
                .IsRequired();
            
            Property(e => e.Description)
                .HasColumnName("EXP_DESCRIPTION");
            
            Property(e => e.StartDate)
                .HasColumnName("EXP_STARTDATE")
                .IsRequired();
            
            Property(e => e.EndDate)
                .HasColumnName("EXP_ENDDATE")
                .IsRequired();

            HasRequired(e => e.Employee)
                .WithMany()
                .HasForeignKey(e => e.EmployeeID);
        }
    }
}
