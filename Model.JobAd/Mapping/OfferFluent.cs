﻿using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Mapping
{
    public class OfferFluent : EntityTypeConfiguration<Offer>
    {
        public OfferFluent()
        {
            ToTable("APP_OFFER");
            HasKey(o => o.ID);

            Property(o => o.ID)
                .HasColumnName("OFF_ID")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(o => o.JobTitle)
                .HasColumnName("OFF_JOBTITLE")
                .IsRequired()
                .HasMaxLength(75);

            Property(o => o.Date)
                .HasColumnName("OFF_DATE")
                .IsRequired();

            Property(o => o.Salary)
                .HasColumnName("OFF_SALARY")
                .IsRequired();

            Property(o => o.City)
                .HasColumnName("OFF_CITY")
                .IsRequired();

            Property(o => o.Description)
                .HasColumnName("OFF_DESCRIPTION")
                .IsRequired();

            Property(o => o.Manager)
                .HasColumnName("OFF_MANAGER");

            HasRequired(o => o.Status)
                .WithMany()
                .HasForeignKey(o => o.StatusID);
        }
    }
}
