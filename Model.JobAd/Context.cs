﻿using Model.JobAd.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd
{
    public class Context : DbContext
    {
        public Context() : base("name=JobAdDatabase")
        {
            // To delete/create all the time
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<Context>());

            // At tthe end (Database don't change)
            //Database.SetInitializer<Context>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }

        public IDbSet<Employee> Employees { get; set; }

        public IDbSet<Experience> Experiences { get; set; }
        
        public IDbSet<Offer> Offers { get; set; }

        public IDbSet<Postulation> Postulations { get; set; }
        
        public IDbSet<Training> Trainings { get; set; }

        public IDbSet<Status> Status { get; set; }
    }
}
