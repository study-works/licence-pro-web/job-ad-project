﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Entity
{
    public class Status
    {
        public int ID { get; set; }

        public String Wording { get; set; }

        public ICollection<Offer> Offers { get; set; }
    }
}
