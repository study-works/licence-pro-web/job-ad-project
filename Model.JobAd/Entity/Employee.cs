﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Entity
{
    public class Employee
    {
        public int ID { get; set; }

        public String LastName { get; set; }

        public String FisrtName { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime EmploymentDate { get; set; }

        public float Salary { get; set; }

        public String Biography { get; set; }

        public ICollection<Postulation> Postulations { get; set; }

        public ICollection<Experience> Experiences { get; set; }

        public ICollection<Training> Trainings { get; set; }
    }
}
