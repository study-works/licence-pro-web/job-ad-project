﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Entity
{
    public class Offer
    {
        public int ID { get; set; }

        public String JobTitle { get; set; }

        public DateTime Date { get; set; }

        public float Salary { get; set; }

        public String City { get; set; }

        public String Description { get; set; }

        public String Manager { get; set; }

        public int StatusID { get; set; }

        public Status Status { get; set; }
    }
}
