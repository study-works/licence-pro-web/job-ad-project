﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Entity
{
    public class Postulation
    {
        public int OfferID { get; set; }

        public int EmployeeID { get; set; }

        public DateTime Date { get; set; }

        public String Status { get; set; }

        public Offer Offer { get; set; }

        public Employee Employee { get; set; }
    }
}
