﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.JobAd.Entity
{
    public class Experience
    {
        public int ID { get; set; }

        public String JobTitle { get; set; }

        public String Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int EmployeeID { get; set; }

        public Employee Employee { get; set; }
    }
}
